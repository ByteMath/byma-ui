import flowbite from 'flowbite/plugin';
import * as path from "path";

// This is a very hacky way to get the proper base path for the library,
// as we need to pass it to Tailwind to scan for Tailwind classes. Normally,
// we would "just" pass a reference to node_modules, but since we're using
// Yarn PnP that doesn't work.
const flowbitePath = path.dirname(require.resolve("flowbite/plugin"));

module.exports = {
  darkMode: 'class',
  content: [
    "src/**/*.{ts,html,css,scss}", 
    "index.html",
    `${flowbitePath}/**/*.js`,
  ],
  theme: {
    fontFamily: {
        sans: ['Inter', 'sans-serif']
    },
    extend: {
        colors: {
            "fmf": {
                DEFAULT: "#173494",
                'dark': "#173494",
            }
        },
    },
},
  plugins: [
    require('@tailwindcss/forms'),
    flowbite,
  ],
};
