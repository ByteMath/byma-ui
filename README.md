
# bymait/ui (Web Components)

bymait/ui is an Open Source UI Component Library to build customizable websites and web applications. It is built with [Lit Elements](https://lit.dev) and Tailwindcss.

## Installation

1. Clone project from GitLab: https://gitlab.com/ByteMath/byma-ui
2. `npm install`

## How to create a new component

1. Create a file "src/my-element/my-element.ts"
2. Create a file "src/my-element/index.ts" with the content:

```
export { MyElement } from '/my-element';
```

3. Add the following to the file src/index.ts:

```
export { MyElement } from './my-element/my-element';
```

4. In vite.config.js add `'my-element': src/my-element/index.ts'`.

5. Use `byma-my-element` in index.html.

## Development

Start development server:

`npm run dev` 

Watch CSS class changes and generate new treeshaked tailwind:

`npm run generate-tailwind`

Start Storybook:

`npm run storybook`

## Publish

1. `npm run build`
2. `npm run publish`

## Integration

### In CMS like Wordpress, Drupal and alike

Add a **Custom HTML** block to your page with the following lines of code:

```html
<script type="module" src="https://unpkg.com/@bymait/ui/dist/entry-index.js"></script>

<!-- The custom web component you want to insert -->
```

![Wordpress Editor](src/shared/assets/integrate-with-wordpress.png)

### In HTML via CDN

```html
<!-- HTML -->
<script type="module" src="https://unpkg.com/@bymait/ui/dist/entry-index.js"></script>

<!-- Or specify version -->
<script type="module" src="https://unpkg.com/@bymait/ui@0.0.3/dist/entry-index.js"></script>

<!-- Or load only specific web component -->
<script type="module" src="https://unpkg.com/@bymait/ui/dist/entry-folder.js"></script>

<!-- Finally use custom tag -->
<byma-folder></byma-folder>
```

### In NPM project via NPM

1. In Terminal or Console:

`npm install @bymait/ui`

2. In HTML

```html
<!-- HTML -->
<script type="module" src="node_modules/@bymait/ui/dist/entry-index.js"></script>

<!-- Or load only specific web component -->
<script type="module" src="node_modules/@bymait/ui/dist/entry-breadcrumbs.js"></script>

<!-- Finally use custom tag -->
```

### In React project

1. In Terminal or Console:

`npm install @bymait/ui @lit/react`

2. In JSX

https://lit.dev/docs/frameworks/react/

```jsx
import React from 'react';
import {createComponent} from '@lit/react';
import {BymaElement} from './byma-element.js';

export const BymaElementComponent = createComponent({
  tagName: 'byma-element',
  elementClass: BymaElement,
  react: React,
  events: {
    onactivate: 'activate',
    onchange: 'change',
  },
});
```

3. Use React component

```jsx
<BymaElementComponent
  active={isActive}
  onactivate={(e) => setIsActive(e.active)}
  onchange={handleChange}
/>
```

## Further reading

- Inspired by [shadcn UI](https://ui.shadcn.com/) and [Umbraco UI Library](https://uui.umbraco.com/)
- Based on: [butopen/web-components-tailwind-starter-kit](https://github.com/butopen/web-components-tailwind-starter-kit)
