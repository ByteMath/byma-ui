/** @type {import('vite').UserConfig} */
import {defineConfig} from "vite";

export default defineConfig(({ command, mode, isSsrBuild, isPreview }) => {
    if (command === 'serve') {
        return {
            // dev specific config
        }
    } else {
        // command === 'build'
        return {
            // build specific config
            root: '',
            build: {
                outDir: './dist',
                rollupOptions: {
                    input: {
                        'index': 'src/index.ts',
                        'accordion': 'src/accordion/index.ts',
                        'carousel': 'src/carousel/index.ts',
                        'personal-information-form': 'src/personal-information-form/index.ts',
                        'breadcrumbs': 'src/breadcrumbs/index.ts',
                        'folder': 'src/folder/index.ts'
                        // Add -> 'template': 'src/template/index.ts'
                    },
                    output: {
                        entryFileNames: `entry-[name].js`,
                        chunkFileNames: `[name].js`,
                        assetFileNames: `[name].[ext]`
                    }
                }
            }
        }
    }
})