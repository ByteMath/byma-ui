import type { WebComponentsRenderer} from "@storybook/web-components";
import { withThemeByClassName } from "@storybook/addon-themes";
import type { Preview } from '@storybook/react';

const preview: Preview = {
  decorators: [
    withThemeByClassName<WebComponentsRenderer>({
        themes: {
            "Light": "",
            "Dark": "dark",
        },
        defaultTheme: "Light"
    })
  ],
  parameters: {
    actions: { argTypesRegex: "^on[A-Z].*" },
    controls: {
      matchers: {
        color: /(background|color)$/i,
        date: /Date$/i,
      },
    },
  },
};

export default preview;
