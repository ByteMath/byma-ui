import { create } from "@storybook/theming/create";
import Logo from "../assets/ByteMath_me.svg";

export default create({
    base: "light",
    brandTitle: "ByMa Components",
    brandUrl: "https://bytemath.lorenzozambelli.it/",
    brandImage: Logo,
    brandTarget: "_self",

    barSelectedColor: "#173494",
})
