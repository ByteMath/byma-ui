import {Meta, StoryObj} from '@storybook/web-components';
import {html} from "lit";
import "./template";
import {TemplateProps} from "./template";

export default {
  title: 'components/Template',
  tags: ['autodocs'],
  render: (args) => html`
    <byma-template
      .myProperty="${args.myProperty}"
    ></byma-template>
  `,
} satisfies Meta<TemplateProps>;

export const Default: StoryObj<TemplateProps> = {
  args: {
    myProperty: null,
  },
};
