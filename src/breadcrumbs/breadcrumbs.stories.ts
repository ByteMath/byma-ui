import { Meta, StoryObj } from "@storybook/web-components";
import { Breadcrumbs, BreadcrumbItem } from "./Breadcrumbs";

const meta = {
    component: "byma-breadcrumbs",
    title: "components/Breadcrumbs",
    tags: [ "autodocs" ],
} satisfies Meta<Breadcrumbs>;
export default meta;

type Story = StoryObj<Breadcrumbs>;
export const Default = {
    parameters: {
        docs: {
            description: {
                story: "Defaul ByMa Breadcrumbs",
            },
        },
    },
} satisfies Story;

export const Custom = {
    args: {
        breadcrumbs: [
            new BreadcrumbItem("Test1", "www.fmf.nl"),
            new BreadcrumbItem("Test2", "www.fmf.nl", "fa-solid fa-book-skull"),
            new BreadcrumbItem("Test3", "www.fmf.nl")
        ]
    },
    parameters: {
        docs: {
            description: {
                story: "Custom BreadcrumbItem layout.",
            },
        },
    },
} satisfies Story;