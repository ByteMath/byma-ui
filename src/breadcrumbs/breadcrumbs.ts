import { customElement, property } from "lit/decorators.js";
import { repeat } from "lit/directives/repeat.js";
import {TailwindElement} from "../shared/tailwind.element";
import { html } from "lit";

/**
 * Represents a breadcrumb navigation component. Breadcrumbs are a type of secondary navigation scheme that reveals the user's location within a website or application.
 * 
 * The `Breadcrumbs` component displays a list of breadcrumb items, each representing a step in the user's navigation path. It provides links to navigate back to previous pages or sections within the site.
 */

@customElement("byma-breadcrumbs")
export class Breadcrumbs extends TailwindElement() {

    /**
     * An array of breadcrumb items to be displayed in the breadcrumb navigation. Each breadcrumb item represents a step in the user's navigation path.
     * A `BreadcrumbItem` contains information about a specific step in the user's navigation path. It consists of a name, a URL, and an optional icon. * Constructs a new BreadcrumbItem.
     * 
     * name {string} - The name or label of the breadcrumb item.
     * 
     * url {string}- The URL associated with the breadcrumb item.
     * 
     * icon {string}- The icon associated with the breadcrumb item, if any. Default is undefined.
     */
    @property({ type: Array })
    public breadcrumbs: Array<BreadcrumbItem> = [
        new BreadcrumbItem("Test1", "www.fmf.nl"),
        new BreadcrumbItem("Test2", "www.fmf.nl"),
    ];

    /**
     * The URL of the home page. This property specifies the URL to which the user is directed when clicking on the "Home" breadcrumb.
     */
    @property({ type: String })
    public home: string = "www.fmf.nl";

    protected override render() {
        return html`
            <nav class="flex py-2">
                <ol class="inline-flex justify-stretch items-center">
                    ${repeat(
                        [new BreadcrumbItem("Home", this.home, "fa-solid fa-house-chimney"), ...this.breadcrumbs],
                        (item) => item.name,
                        (item) => html`
                            <li>
                                <div class="flex items-center">
                                    <!-- Chevron -->
                                    ${item.url === this.home && item.name == "Home" ? "" : html`
                                        <svg class="w-3 h-3 grow-0 text-gray-400" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 6 10">
                                            <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="m1 9 4-4-4-4"/>
                                        </svg>
                                    `}

                                    <!-- Navigation item itself -->
                                    <a href="${item.url}" class="text-gray-700 text-opacity-80 hover:text-opacity-100 dark:text-fmf mx-2 font-medium md:mx-4 transition-all ease-in-out hover:-mt-2 hover:pb-2">
                                        ${item.icon ? html`<i class="${item.icon} mr-2"></i>` : ""}
                                        <span class="text-sm inline-block font-medium">${item.name}</span>
                                    </a>
                                </div>
                            </li>
                        `
                    )}
                </ol>
            </nav>
        `;
    }
}

declare global {
    interface HTMLElementTagNameMap {
        "byma-breadcrumbs": Breadcrumbs;
    }
}

export class BreadcrumbItem {
    constructor(
        public name: string,
        public url: string,
        public icon?: string
    ) {}
}
