import { customElement, property } from "lit/decorators.js";
import { repeat } from "lit/directives/repeat.js";
import { initDropdowns } from "flowbite";
import {TailwindElement} from "../shared/tailwind.element";
import { PropertyValueMap, html } from "lit";
import style from './folder.css'

/**
 * The `Folder` class represents a custom element for a folder component with dynamic items.
 * 
 */
@customElement("byma-folder")
export class Folder extends TailwindElement(style) {
    /**
     * An array of folder items to be displayed as menu and their respective menu. Each column contains a collection of FooterItem objects, representing individual items within the column.
     * 
     * A `FolderMenu` consists of a name, an array of items. Constructs a new FolderMenu.
     * 
     * name (string) - The name of the folder.
     * 
     * items ({Array<FolderItem>}) - An array of items in the folder menu.
     * 
     * showInitial (boolean) - If set to false the first character of the name will not appear on the folder. Default True.
     * 
     * A `FolderItem` can contain a name, a URL, and an label. Constructs a new FolderItem.
     * 
     * label (string) - The name associated with the item. 
     * 
     * url (string) - The URL of the item.
     * 
     * iconName (string) - The string with the name of the icon type of the item,if any. Default is folder.
     * 
     */
    @property({ type: Array })
    public menu: Array<FolderMenu> = [
        new FolderMenu(
            "F", 
            [
                new FolderItem("FMF", "https://www.youtube.com/watch?v=dQw4w9WgXcQ&list=RDdQw4w9WgXcQ&start_radio=1"),
            ],
        ),
    ];
   
    protected override updated(_changedProperties: PropertyValueMap<any> | Map<PropertyKey, unknown>): void {
        initDropdowns();
    }
    
    protected override render() {
        return html`
        <ul class="flex flex-wrap max-w-screen-md" >
        ${repeat(
                this.menu,
                (item) => item.name, 
                (item) => html`
                <li class="folder p-10 sm:w-[1vw] md:w-[3vw] lg:w-[6vw] relative group">
                    <div style="position: absolute;">
                        <button
                            id="dropdownDelayButton-${item.name}"
                            data-dropdown-toggle="dropdownDelay-${item.name}"
                            data-dropdown-delay="100"
                            data-dropdown-trigger="hover"
                            type="button"
                            class="relative flex flex-col items-center group:"
                        >
                            <i style="font-size: 8vh; margin-bottom: ${item.name.length > 1 ? '0.5vh' : '0'}" class="up fa-solid fa-folder-closed big text-fmf p-2"></i>
                            
                            ${item.showInitial == true ? html`
                                <span class="fa-layers-text fa-inverse absolute transform -translate-x-1/2 -translate-y-1/2 inset-6 lg:py-[2vh] sm:py-[3vw] md:py-[4vh] py-[2.5vh] flex items-center justify-center transition-transform scale-100  group-hover:skew-x-[-30deg]" style="font-weight: 900; font-size: 3vh; top: 60%; left: 50%">${item.name.charAt(0)}</span>
                            ` : html``}

                            ${item.name.length > 1 ? html`
                                    <!-- If name is longer than 1 character, display on max 1 lines with ellipsis -->
                                    <span class="font-bold absolute py-[10vh] overflow-hidden max-lines-1 w-[10vw] lg:w-[6.4vw]" style="font-size: 2vh; text-align: center; display: block; line-height: 2.2vh; white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">${item.name}</span>
                                ` : html``}
                        </button>
                    </div>
                    <div id="dropdownDelay-${item.name}" class="absolute w-60 px-5 py-3 dark:bg-gray-800 bg-white rounded-lg shadow border dark:border-transparent mt-5 z-[10000] hidden"> 
                        <ul class="space-y-3 dark:text-white">
                            ${repeat(
                                item.items,
                                (subItem) => subItem.label, // Use a unique identifier for the key
                                (subItem) => html`
                                    <li class="font-medium subfolder">
                                        <a href=${subItem.url} class="flex items-center transform transition-colors duration-200 border-r-4 border-transparent hover:border-fmf-dark">
                                            <div class="mr-3">
                                                <i class="${subItem.iconName == "False"? "sub fa-solid fa-folder-closed": subItem.iconName} w-6 h-6 dark:text-white text-fmf-dark"></i>
                                            </div>
                                            ${subItem.label}
                                        </a>
                                    </li>
                                `
                            )}
                        </ul>
                    </div>
                </li>
                `
            )}
            </ul>
            
        `;
    }
}                       
declare global {
    interface HTMLElementTagNameMap {
        "byma-folder": Folder;
    }
}
export class FolderItem {
    constructor(
        public label: string,
        public url: string,
        public iconName: string = "False",
    ) { }
}
export class FolderMenu {
    constructor(
        public name: string,
        public items: Array<FolderItem> = [],
        public showInitial: boolean = true,
    ) {}
}
