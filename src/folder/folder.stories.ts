import { Meta, StoryObj } from "@storybook/web-components";
import { FolderMenu, FolderItem, Folder } from "./folder";

const meta = {
    component: "byma-folder",
    title: "Components/Folder",
    tags: [ "autodocs" ],
    parameters: {
        docs: {
            description: {
                component: `This component provides a dynamic menu characterize by folder icons. Each menu can infinite amount of folder, and each folder can have infinite amount of links. The user has the possibility to personalize the icons of each links.`,
            },
        },
    },
} satisfies Meta<Folder>;
export default meta;

type Story = StoryObj<Folder>;

export const Default = {
    parameters: {
        docs: {
            description: {
                story: "The default component.",
            },
        },
    },
} satisfies Story;

export const Custom = {
    parameters: {
        docs: {
            description: {
                story: "In here some examples on the possible personalizations. If the name is longer than one character, it will appear below the folder, but without overflow. Max 1 line. ",
            },
        },
    },
    args: {
        menu: [
            new FolderMenu(
                "FMF", [
                    new FolderItem("FMF", "https://www.youtube.com/watch?v=dQw4w9WgXcQ&list=RDdQw4w9WgXcQ&start_radio=1"),
                    new FolderItem("FMF", "https://www.youtube.com/watch?v=dQw4w9WgXcQ&list=RDdQw4w9WgXcQ&start_radio=1"),
                ]
            ),
            new FolderMenu(
                "Fysisch-Mathematische Faculteitsvereniging", 
                [
                    new FolderItem("FMF", "https://www.youtube.com/watch?v=dQw4w9WgXcQ&list=RDdQw4w9WgXcQ&start_radio=1", "fa-regular fa-face-laugh-wink"),
                ]
            ),
            new FolderMenu(
                "FM", [
                    new FolderItem("FMF", "https://www.youtube.com/watch?v=dQw4w9WgXcQ&list=RDdQw4w9WgXcQ&start_radio=1"),
                    new FolderItem("FMF", "https://www.youtube.com/watch?v=dQw4w9WgXcQ&list=RDdQw4w9WgXcQ&start_radio=1"),
                ],
                false,
            ),
            new FolderMenu(
                "Mathematische", 
                [
                    new FolderItem("FMF", "https://www.youtube.com/watch?v=dQw4w9WgXcQ&list=RDdQw4w9WgXcQ&start_radio=1", "fa-regular fa-face-laugh-wink"),
                ]
            ),
        ]
    }
} satisfies Story;